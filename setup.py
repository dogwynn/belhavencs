from distutils.core import setup

setup(
    name = 'belhavencs',
    packages = ['belhavencs'],
    install_requires=[
        'ruamel.yaml',
        'gems',
        'tokenmanager',
        'future',
    ],
    # package_dir={'belhavencs': 'belhavencs'},
    version = '0.0.1',
    description = 'Library of utilities Belhaven University uses for computer science classes',
    author="David O'Gwynn",
    author_email="dogywnn@belhaven.edu",
    url = 'https://bitbucket.org/dogwynn/belhavencs',

    classifiers=[
        'Programming Language :: Python',
    ],
)
