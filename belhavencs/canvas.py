from pathlib import Path
from collections import namedtuple

from tokenmanager import get_tokens

try:
    import canvasapi
except ImportError:
    class canvasapi:
        pass
    canvasapi.Course = object

from .students import StudentDB
from .logging import new_log
from .utils import walk
from . import yaml
# from .accounts import get_accounts

_log = new_log('canvas')

CourseID = namedtuple('CourseID',['year','period','id','section','name'])

class BelhavenCourse(canvasapi.Course):
    @property
    def id(self):
        course = CourseID(-1, '', '', '', '')
        try:
            name = self['name']
            cid = name.split(maxsplit=1)[0].lower()
            section = name.rsplit(maxsplit=1)[-1].lower()
            long_name = name.split(maxsplit=1)[-1].rsplit(maxsplit=1)[0]
            term = self['term']['name']
            period, year = [s.lower() for s in term.split()]
            year = int(year)
            course = CourseID(year,period,cid,section,long_name)
        except:
            _log.error('Could not parse id for course: %s',self['name'])
            raise
        finally:
            return course

def find_course(root):
    root = Path(root).expanduser().resolve()
    paths = list(walk(root, pattern='course.yml'))
    course_info = yaml.read_yaml(paths[0])
    if len(paths)>1:
        raise IOError('Must provide root with only one unique course.yml file')
    api = get_api()
    return api.course(course_info['canvas_id'])

def get_course(api, course_db):
    return api.course(course_db['canvas_id'])
    
def get_api():
    # accounts = get_accounts()
    return canvasapi.CanvasAPI(
        'https://belhaven.instructure.com/api/v1',
        course_class=BelhavenCourse,
        token=get_tokens().canvas,
    )
