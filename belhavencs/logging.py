''' logging utilities built on top of Python's logging module
'''
import os
import sys
import logging

# ---------------------------------------------------------------------------
# Logging
# ---------------------------------------------------------------------------

LOG_FORMAT = '{asctime} {module}:{lineno} {levelname: <8} {message}'
DATEFMT = '%Y-%m-%dT%H:%M:%S'

def new_log(name):
    log = logging.getLogger(name)
    log.addHandler(logging.NullHandler())

    return log
    

def setup_logging(level='info'):
    level = logging.getLevelName(level.upper())
    logging.basicConfig(level=level, datefmt=DATEFMT, format=LOG_FORMAT,
                        style='{')


