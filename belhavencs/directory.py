from pathlib import Path
from collections import OrderedDict
import html as _html
import json
import urllib
import copy
import hashlib
import datetime
import random
import types
from functools import lru_cache
import pprint

import jinja2
import markdown
from gems import composite
from ruamel.yaml.comments import CommentedMap
import bs4

try:
    import canvasapi
except:
    class canvasapi: pass
    canvasapi.CanvasMarkdown = object

from . import yaml
from .students import StudentDB
from .logging import new_log
from .utils import walk

memoize = lru_cache(None)

log = new_log('directory')

def find_file(name):
    here = Path('.').absolute()
    paths = list(
        filter(lambda p:p.joinpath(name).exists(),
               [here] + list(here.parents))
    )
    if paths:
        return Path(paths[0],name)

def get_paths(paths):
    return [Path(p).expanduser().resolve() for p in paths]

class CourseDirectories:
    def __init__(self, course, root_dir):
        self._term_dir_name = '{}-{}'.format(course.id.year,course.id.period)
        self._section_dir_name = course.id.section
        self._dirs, self._files = OrderedDict(), OrderedDict()
        self._dirs['root'] = Path(root_dir).expanduser().resolve()
        self._dirs['course'] = Path(self._dirs['root'], course.id.id)
        self._dirs['slides'] = Path(self._dirs['course'], 'slides')
        self._dirs['pages'] = Path(self._dirs['course'], 'pages')
        self._dirs['assignments'] = Path(self._dirs['course'], 'assignments')
        self._files['assignment_groups'] = Path(self._dirs['assignments'],
                                                'groups.yml')
        self._dirs['modules'] = Path(self._dirs['course'], 'modules')
        self._files['module_master'] = Path(
            self._dirs['modules'], 'modules.yml'
        )
        self._dirs['quizzes'] = Path(self._dirs['course'], 'quizzes')
        self._dirs['cloud'] = Path(self._dirs['course'], 'cloud')
        # self._dirs['questions'] = Path(self._dirs['quizzes'], 'questions')
        self._dirs['term'] = Path(self._dirs['course'],self._term_dir_name)
        self._dirs['section'] = Path(self._dirs['term'], self._section_dir_name)
        self._files['syllabus'] = Path(self._dirs['section'], 'syllabus.md')

    @property
    def directories(self):
        return list(self._dirs.values())

    @property
    def files(self):
        return list(self._files.values())

    @property
    def course_dir(self):
        return Path(self._dirs['course'])

    @property
    def root_dir(self):
        return Path(self._dirs['root'])

    @property
    def db_path(self):
        return Path(self._dirs['section'], 'course.yml')

    @property
    def slides_html(self):
        return list(Path(self._dirs['slides']).glob('slide-*.html'))
    @property
    def slides_md(self):
        return [p for p in Path(self._dirs['slides']).glob('slide-*.md')]

    def metadata_path(self, path, create=True):
        path = Path(path).expanduser().resolve()
        md_path = Path(path.parent, 'metadata',
                       self._term_dir_name, self._section_dir_name)
        if create:
            if not md_path.exists():
                log.info('Creating metadata directory {}'.format(md_path))
                md_path.mkdir(parents=True)
        return Path(md_path, path.name + '.metadata.yml')

    @property
    def pages(self):
        paths = list(Path(self._dirs['pages']).glob('page-*.md'))
        return paths

    @property
    def assignments(self):
        return list(Path(self._dirs['assignments']).glob('assign-*.md'))

    @property
    def assignment_groups(self):
        return Path(self._files['assignment_groups'])

    @property
    def quizzes(self):
        return list(Path(self._dirs['quizzes']).glob('quiz-*.yml'))

    @property
    def modules(self):
        return list(Path(self._dirs['modules']).glob('module-*.yml'))

    @property
    def modules_master(self):
        return Path(self._files['module_master'])

    @property
    def syllabus_md(self):
        return Path(self._files['syllabus'])

    @property
    def cloud_path(self):
        return self._dirs['cloud']

class CourseDbPathError(Exception):
    pass

class CourseDB(CommentedMap):
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        if not (a or kw):
            self['year'] = ''
            self['period'] = ''
            self['id'] = ''
            self['section'] = ''
            self['canvas_id'] = ''
            self['students'] = []
        
    @classmethod
    def from_path(cls, path):
        return cls(yaml.read_yaml(path))
    @classmethod
    def find_dbs(cls, path, unique=False):
        path = Path(path).expanduser().resolve()
        paths = list(walk(path, pattern='course.yml'))
        for p in path.parents:
            p = Path(p, 'course.yml')
            if p.exists():
                paths.append(p)
        if len(paths)>1 and unique:
            raise CourseDbPathError(
                'Path: {p} resolves to more than one'
                ' unique course.yml file'.format(p=path)
            )
        elif not paths:
            raise CourseDbPathError(
                'Path: {p} has no course.yml file'.format(p=path)
            )
        return [cls.from_path(p) for p in paths]
        

class CourseManagement:
    def __init__(self, course, root_dir, dry_run=False, force_upload=False):
        self.course = course
        self.dirs = CourseDirectories(course, root_dir)
        self.dry_run = dry_run
        self.force_upload = force_upload
        self.files_to_upload = set()

        self.db = CourseDB((
            ('year', course.id.year),
            ('period', course.id.period),
            ('id', course.id.id),
            ('section', course.id.section),
            ('canvas_id', course['id']),
            ('students', []),
        ))

        self.random = random.Random(course['id'])

        self.students_db = StudentDB(root_dir)

    def set_random_salt(self, salt):
        print((self.course['id'], salt))
        self.random = random.Random((self.course['id'], salt))

    def initialize(self):
        for path in self.dirs.directories:
            if not path.exists():
                log.info('Creating dir: %s %s', path,path.exists())
                if not self.dry_run:
                    path.mkdir(parents=True,exist_ok=True)
                else:
                    log.info('  ... DRY RUN ... not done.')

        for s in self.course.students:
            smap = self.students_db.add_from_canvas(s)
            self.db['students'].append(smap)

        log.info('Syncing student database...')
        if not self.dry_run:
            self.students_db.sync()
        else:
            log.info('  ... DRY RUN ... not done.')

        log.info('Writing YAML to: %s', self.dirs.db_path)
        if not self.dry_run:
            yaml.write_yaml(self.db, self.dirs.db_path)
        else:
            log.info('  ... DRY RUN ... not done.')

    def sync_all(self):
        self.sync_slides()
        self.sync_pages()
        self.sync_assignments()
        self.sync_syllabus()
        # self.sync_modules()

    # def _attr_from_path(self, path, attr):
    #     html = self._md.mdconvert(self.template(path))
    #     return html.metadata[attr]

    meta_id = 'canvasapi-metadata'
    def tag_html(self, html, **meta):
        div = '<div id="{0}" style="display: none">{1}</div>'.format(
            self.meta_id, _html.escape(json.dumps(meta))
        )
        return html + '\n' + div
    def get_node_tag(self, node, attr):
        bs = bs4.BeautifulSoup(node[attr], 'html.parser')
        meta = bs.find(id=self.meta_id)
        if meta is not None:
            return json.loads(_html.unescape(meta.text))

    def hash(self, content):
        return hashlib.sha256(content).hexdigest()

    def canvas_metadata(self, canvas_dict, content):
        '''Given a Canvas object (canvas_dict) and local file content,
        return a metadata dict to be saved to file.

        Args:
          canvas_dict (dict): Dictionary returned by Canvas API operations
          content (bytes): Content to be hashed. Must be encoded to bytes.

        Keys added to metadata dict:

        - local_hash (str): hash of local file content

        '''
        metadata = copy.deepcopy(canvas_dict)
        metadata['local_hash'] = self.hash(content)
        return CommentedMap(sorted(metadata.items()))
        
    _md = canvasapi.CanvasMarkdown()

    def _dt_to_str(self, node):
        new = {}
        for key, value in list(node.items()):
            if isinstance(value, datetime.datetime):
                new[key] = str(value)
            else:
                new[key] = value
        return new

    def sync_slides(self, paths=None):
        '''Sync the local slide with what is stored on Canvas.

        - Slide file that has already been synced should have a
          .metadata.yml file 

        - Slide files without a .metadata.yml file is new and should be
          created

        - Slides with a .metadata.yml but with no corresponding Canvas slide
          should be recreated
        
        - Slides with a .metadata.yml and a corresponding Canvas slide
          should be checked to see if they need to be updated.

        - To check a slide, take SHA256 hash of slide file, compare
          with hash stored in .metadata.yml file.

        '''
        get_name = lambda d: d['filename']
        get_id = lambda d: d['id']
        slide_paths = get_paths(paths) if paths else self.dirs.slides_html
        slide_metas = [self.dirs.metadata_path(p) for p in slide_paths]
        canvas_files = self.course.files
        
        new_slides = []
        slides_to_update = []
        for path, meta_path in zip(slide_paths, slide_metas):
            if meta_path.exists():
                # If the .metadata.yml file exists, read it
                meta = yaml.read_yaml(meta_path)
                matching = list(filter(
                    lambda a:get_id(a)==get_id(meta), canvas_files
                ))

                if matching:
                    canvas_file = matching[0]
                    local_hash = self.hash(path.read_bytes())

                    if meta['local_hash'] != local_hash:
                        # If the hash in the .metadata.yml is different
                        # than the hash of the local .md file, then we
                        # need to update
                        log.info('Local file has changed. Updating'
                                 ' %s (%s)', get_name(meta),
                                 get_id(meta))
                        slides_to_update.append((path, meta, meta_path))

                else:
                    # .metadata.yml file with no corresponding canvas
                    # file. Remove local metadata and recreate
                    # file.
                    log.info('No matching Canvas file for %s. '
                             'Deleting local cache',
                             get_name(meta))
                    meta_path.unlink()
                    new_slides.append((path, meta_path))

            else:
                # This is a new file
                log.info('New file found: %s', path.name)
                new_slides.append((path, meta_path))

        if not (new_slides or slides_to_update):
            log.info('No slides needing updates')

        for path, meta_path in new_slides:
            # Create the new slides
            #
            log.info('Creating new file: %s', path)
            if not self.dry_run:
                new_file = self.course.upload_file(path, '/slides')
                # Augment the canvas dictionary returned with the
                # necessary metadata
                meta = self.canvas_metadata(new_file, path.read_bytes())
                yaml.write_yaml(meta, meta_path)
            else:
                log.info('  ... DRY RUN ... not done.')

        for path, meta, meta_path in slides_to_update:
            # path is the local file path and the meta is the
            # previously stored metadata that is now out of phase and
            # requires updating
            log.info('Updating file: %s (%s)',
                     get_name(meta), get_id(meta) )
            if not self.dry_run:
                canvas_file = self.course.upload_file(path, '/slides')
                meta = self.canvas_metadata(canvas_file, path.read_bytes())
                yaml.write_yaml(meta, meta_path)
            else:
                log.info('  ... DRY RUN ... not done.')


            

    def sync_pages(self, paths=None):
        '''Sync the local page markdown with what is stored on Canvas.

        - Page .md file that has already been synced should have a
          .metadata.yml file

        - Page .md files without a .metadata.yml file is new and should be
          created

        - Pages with a .metadata.yml but with no corresponding Canvas page
          should be recreated
        
        - Pages with a .metadata.yml and a corresponding Canvas page
          should be checked to see if they need to be updated.

        - To check a page, take SHA256 hash of .md file, compare with
          hash stored in .metadata.yml file.

        '''
        get_name = lambda d: d['title']
        get_id = lambda d: d['page_id']

        page_paths = get_paths(paths) if paths else self.dirs.pages
        page_metas = [self.dirs.metadata_path(p) for p in page_paths]
        canvas_pages = self.course.pages

        new_pages = []
        pages_to_update = []
        for path, meta_path in zip(page_paths, page_metas):
            if meta_path.exists():
                # If the .metadata.yml file exists, read it
                meta = yaml.read_yaml(meta_path)
                matching = list(filter(
                    lambda a:get_id(a)==get_id(meta), canvas_pages
                ))

                if matching:
                    canvas_page = matching[0]
                    local_hash = self.hash(self.template(path).encode())

                    if meta['local_hash'] != local_hash:
                        # If the hash in the .metadata.yml is different
                        # than the hash of the local .md file, then we
                        # need to update
                        log.info('Local markdown has changed. Updating'
                                 ' page %s (%s)', get_name(meta),
                                 get_id(meta))
                        pages_to_update.append((path, meta, meta_path))

                else:
                    # .metadata.yml file with no corresponding canvas
                    # page. Remove local metadata and recreate
                    # page.
                    log.info('No matching Canvas page for %s. '
                             'Deleting local cache',
                             get_name(meta))
                    meta_path.unlink()
                    new_pages.append((path, meta_path))

            else:
                # This is a new page
                log.info('New page found: %s', path.name)
                new_pages.append((path, meta_path))

        for path, meta_path in new_pages:
            # Create the new pages
            #
            # Convert templated markdown to html
            log.debug('Parsing new page markdown: %s', path)
            html = self._md.mdconvert(self.template(path))
            # html.metadata is the embedded YAML in the markdown that
            # corresponds to keyword arguments for the new_page method
            page_kwargs = html.metadata
            page_kwargs['body'] = html
            log.info('Creating new page: %s', get_name(page_kwargs))
            if not self.dry_run:
                new_page = self.course.new_page(**page_kwargs)
                # Augment the canvas dictionary returned with the
                # necessary metadata
                meta = self.canvas_metadata(
                    new_page, self.template(path).encode()
                )
                yaml.write_yaml(meta, meta_path)

        for path, meta, meta_path in pages_to_update:
            # path is the local .md file path and the meta is the
            # previously stored metadata that is now out of phase and
            # requires updating
            log.debug('Parsing existing page markdown: %s', path)
            html = self._md.mdconvert(self.template(path))
            page_kwargs = html.metadata
            page_kwargs['body'] = html
            log.info('Updating page: %s (%s)',
                     get_name(page_kwargs), get_id(meta) )
            page = self.course.page(get_id(meta))
            log.debug(self.course.api)
            page.update(page_kwargs)
            if not self.dry_run:
                page.sync()
                meta = self.canvas_metadata(
                    page, self.template(path).encode()
                )
                if self.files_to_upload:
                    for path in self.files_to_upload:
                        log.info('Uploading file from sync_pages: %s', path)
                        self.course.upload_file(path)
                    self.files_to_upload = set()

                yaml.write_yaml(meta, meta_path)
        
        if not (new_pages or pages_to_update):
            log.info('No pages needing updates')

    def sync_assignments(self, paths=None):
        '''Sync the local assignment markdown with what is stored on Canvas.

        - Assignment .md file that has already been synced should
          have a .metadata.yml file

        - Assignment .md files without a .metadata.yml file is new and
          should be created

        - Assignments with a .metadata.yml but with no corresponding
          Canvas assignment should be recreated
        
        - Assignments with a .metadata.yml and a corresponding Canvas
          assignment should be checked to see if they need to be
          updated.

        - To check an assignment, take SHA256 hash of .md file,
          compare with hash stored in .metadata.yml file.

        '''
        assign_paths = get_paths(paths) if paths else self.dirs.assignments
        assign_metas = [self.dirs.metadata_path(p) for p in assign_paths]
        # Need nonquiz assignments (since quizzes are technically
        # assignments)
        canvas_assigns = self.course.nonquiz_assignments

        new_assignments = []
        assignments_to_update = []
        for path, meta_path in zip(assign_paths, assign_metas):
            if meta_path.exists():
                # If the .metadata.yml file exists, read it
                meta = yaml.read_yaml(meta_path)
                matching = list(filter(lambda a:a['id']==meta['id'],
                                       canvas_assigns))
                if matching:
                    canvas_assign = matching[0]
                    local_hash = self.hash(self.template(path).encode()) 
                    if meta['local_hash'] != local_hash:
                        # If the hash in the .metadata.yml is different
                        # than the hash of the local .md file, then we
                        # need to update
                        log.info('Local markdown has changed. Updating'
                                 ' assignment %s (%s)', meta['name'],
                                 meta['id'])
                        assignments_to_update.append((path, meta, meta_path))
                else:
                    # .metadata.yml file with no corresponding canvas
                    # assignment. Remove local metadata and recreate
                    # assignment.
                    log.info('No matching Canvas assignment for %s. '
                             'Deleting local cache',
                             meta['name'])
                    meta_path.unlink()
                    new_assignments.append((path, meta_path))
            else:
                # This is a new assignment
                new_assignments.append((path, meta_path))

        def assignment_kwargs(kwargs):
            new = kwargs.copy()
            if 'submission_types' in new:
                st_trans = {
                    'text': 'online_text_entry',
                    'url': 'online_url',
                    'upload': 'online_upload',
                }
                new['submission_types'] = [
                    st_trans.get(t,t) for t in new['submission_types']
                ]
            return new

        for path, meta_path in new_assignments:
            # Create the new assignments
            #
            # Convert templated markdown to html
            log.debug('Parsing new assignment markdown: %s', path)
            html = self._md.mdconvert(self.template(path))
            # html.metadata is the embedded YAML in the markdown that
            # corresponds to keyword arguments for the new_assignment
            # method
            assign_kwargs = assignment_kwargs(html.metadata)
            # assign_kwargs = html.metadata
            assign_kwargs.setdefault('name', path.name)
            assign_kwargs['description'] = str(html)
            log.info('Creating new assignment: %s', assign_kwargs['name'])
            if not self.dry_run:
                new_assignment = self.course.new_assignment(**assign_kwargs)
                # Augment the canvas dictionary returned with the
                # necessary metadata
                meta = self.canvas_metadata(
                    new_assignment, self.template(path).encode()
                )
                yaml.write_yaml(meta, meta_path)

        for path, meta, meta_path in assignments_to_update:
            # path is the local .md file path and the meta is the
            # previously stored metadata that is now out of phase and
            # requires updating
            log.debug('Parsing existing assignment markdown: %s', path)
            html = self._md.mdconvert(self.template(path))
            assign_kwargs = assignment_kwargs(html.metadata)
            # assign_kwargs = html.metadata
            assign_kwargs.setdefault('name', path.name)
            assign_kwargs['description'] = html
            log.info('Updating assignment: %s (%s)',
                     assign_kwargs['name'], meta['id'] )
            assignment = self.course.assignment(meta['id'])
            if not self.dry_run:
                assignment.update(assign_kwargs)
                assignment.sync()
                meta = self.canvas_metadata(
                    assignment, self.template(path).encode()
                )
                yaml.write_yaml(meta, meta_path)
                if self.files_to_upload:
                    for path in self.files_to_upload:
                        log.info(
                            'Uploading file from sync_assignments: %s',
                            path
                        )
                        self.course.upload_file(path)
                    self.files_to_upload = set()

        if not (new_assignments or assignments_to_update):
            log.info('No assignments needing updates')

    def _prep_answer(self, answer):
        '''Transform answer dictionary to one suitable for API POST

        '''
        kmap = {
            'comments': 'answer_comments',
            'match_left': 'answer_match_left',
            'ml': 'answer_match_left',
            'match_right': 'answer_match_right',
            'mr': 'answer_match_right',
            'incorrect': 'matching_answer_incorrect_matches',
            'weight': 'answer_weight',
            'w': 'answer_weight',
            'html': 'answer_html',
            'h': 'answer_html',
            'text': 'answer_text',
            't': 'answer_text',
        }

        new = {kmap.get(k,k):v for k,v in answer.items()}
        if 'answer_html' in new:
            new['answer_html'] = str(self._md.mdconvert(
                str(new['answer_html'])
            ))
        if 'answer_text' in new:
            new['answer_text'] = str(new['answer_text'])

        for t in ['answer_match_left', 'answer_match_right']:
            if t in new:
                new[t] = str(new[t])

        def is_matching(a):
            return ('answer_match_left' in a or
                    'matching_answer_incorrect_matches' in a)
        
        need_weight = not is_matching(new)
        
        if need_weight:
            new.setdefault('answer_weight',0)
            if new['answer_weight']:
                new['answer_weight'] = 100
        return new

    def _prep_question(self, question):
        '''Transform question dictionary to one suitable for API POST

        '''
        kmap = {
            'text': 'question_text',
            'type': 'question_type',
            'points': 'points_possible',
            'p': 'points_possible',
            'incorrect': 'matching_answer_incorrect_matches',
            'answer': 'answers',
            'a': 'answers',
        }

        new = {kmap.get(k,k):v for k,v in question.items()}
        new['question_text'] = str(self._md.mdconvert(new['question_text']))
        if 'answer' in new:
            new['answers'] = new.pop('answer')

        new['answers'] = [self._prep_answer(a)
                          for a in question.get('answers',[])]

        if 'question_type' not in new:
            qt = 'multiple_choice_question'
            n_answers = sum(1 if a['answer_weight'] else 0
                            for a in new['answers'])
            if n_answers > 1:
                qt = 'multiple_answers_question'

            new['question_type'] = qt
        qtmap = {
            'tf': 'true_false_question',
            'mc': 'multiple_choice_question',
            'ma': 'multiple_answers_question',
            'match': 'matching_question',
            'mat': 'matching_question',
            'sa': 'short_answer_question',
            'e': 'essay_question',
        }
        qt = new['question_type']
        new['question_type'] = qtmap.get(qt, qt)
        log.debug(new)
        return new

    def _prep_quiz(self, config):
        new = copy.deepcopy(config)
        new['description'] = str(self._md.mdconvert(new['description']))
        questions = config['questions'][:]

        self.set_random_salt(new['title'])
        if 'shuffle_questions' in new:
            if new.pop('shuffle_questions'):
                questions = self.shuffled(questions)

        new['questions'] = [self._prep_question(q) for q in questions]
        # for q in new['questions']:
        #     print(q['question_text'])
        #     print()
        return new

    def sync_quizzes(self, paths=None):
        '''Sync the local quiz markdown with what is stored on Canvas.

        - Quiz .yml file that has already been synced should have a
          metadata file

        - Quiz .yml files without a metadata file is new and should be
          created

        - Quizzes with a metadata but with no corresponding Canvas
          quiz should be recreated
        
        - Quizzes with a metadata and a corresponding Canvas quiz
          should be checked to see if they need to be updated.

        - To check a quiz, take SHA256 hash of .yml file, compare with
          hash stored in metadata file.

        '''
        def config_content(c):
            return yaml.dump(c).encode()
        def config_hash(c):
            return self.hash(config_content(c))
        def question_content(q):
            return yaml.dump(q).encode()
        def question_hash(q):
            return self.hash(question_content(q))

        paths = get_paths(paths) if paths else self.dirs.quizzes
        configs = []
        for p in paths:
            log.info('Loading quiz YAML: %s', p)
            configs.append(self._prep_quiz(yaml.load(self.template(p))))
        meta_paths = [self.dirs.metadata_path(p) for p in paths]

        quizzes = self.course.quizzes

        new_quizzes = []
        quizzes_to_update = []

        for config, meta_path in zip(configs, meta_paths):
            # 601-948-6262
            # Account #: 1702051837
            if meta_path.exists():
                # If metadata exists, read it
                meta = yaml.read_yaml(meta_path)
                matching = list(filter(lambda q:q['id']==meta['id'], quizzes))
                if matching:
                    # Check to see if this quiz has changed
                    quiz = matching[0]
                    local_hash = config_hash(config)
                    if meta['local_hash'] != local_hash:
                        # If the hash in the metadata file is
                        # different than the hash of the local .yml
                        # file, then update
                        log.info('Local YAML has changed. Updating'
                                 ' quiz %s (%s)', meta['title'], meta['id'])
                        quizzes_to_update.append((config, quiz, meta, meta_path))

                else:
                    # Metadata file with no corresponding Canvas
                    # quiz. Remove local metadata and recreate quiz.
                    log.info('No matching Canvas quiz for %s.'
                             ' Deleting local cache.', meta['title'])
                    meta_path.unlink()
                    new_quizzes.append((config, meta_path))

            else:
                # New quiz
                new_quizzes.append((config, meta_path))
                
        for config, meta_path in new_quizzes:
            # Create the new quiz
            #
            # Generate keyword args for new quiz. I.e. all (key,value)
            # pairs except for the 'questions' key. Questions are
            # handled later.
            kwargs =  {k:v for k,v in config.items() if k not in {'questions'}}
            log.info('Creating new quiz: %s', kwargs['title'])
            if not self.dry_run:
                new_quiz = self.course.new_quiz(**kwargs)
                # Create quiz metadata object. Do not write to file
                # until questions have been added.
                meta = self.canvas_metadata(
                    new_quiz, config_content(config)
                )
                quizzes_to_update.append((config, new_quiz, meta, meta_path))
            else:
                log.info('  ... DRY RUN ... not done.')

        for config, quiz, meta, meta_path in quizzes_to_update:
            # config local quiz configuration (read from YAML file),
            # quiz is Canvas object to update, meta is the local cache
            # of the Canvas object, and meta_path is the path to that
            # cached object

            kwargs = {k:v for k,v in config.items() if k not in 'questions'}
            log.info('Updating quiz: %s (%s)', kwargs['title'], quiz['id'])

            if not self.dry_run:
                quiz.update(kwargs)
                quiz.sync()
            else:
                log.info('  ... DRY RUN ... not done.')

            any_question_changed = False

            # Use existing questions (if any) to put local question
            # config data. If there are more local questions than
            # canvas questions, then create new questions. If there
            # are fewer, then destroy the extraneous questions.

            local_questions = config['questions']

            self.set_random_salt(config['title'])
            if config.get('shuffle_questions'):
                local_questions = self.shuffled(local_questions)
                # print()
                print('-'*70)
                print('-'*70)
                for q in local_questions:
                    print(q['question_text'])
                    print()
            canvas_questions = quiz.questions
            # print()
            # print('-'*70)
            # print('-'*70)
            # for q in canvas_questions:
            #     print(q['question_text'])
            #     print()
            question_data = list(zip(canvas_questions,local_questions))

            len_c, len_l = len(canvas_questions), len(local_questions)
            
            if len_c < len_l:
                question_data.extend(
                    (None, lq) for lq in local_questions[len_c:]
                )
            else:
                extraneous = canvas_questions[len_l:]
                if extraneous:
                    log.info('Deleting extraneous canvas questions (%s)',
                             ','.join(map(str,[q['id'] for q in extraneous])))
                    for q in extraneous:
                        q.destroy()

            meta_questions = {q['id']:q for q in meta.get('questions',[])}
            question_metas = []
            for i,(canvas_q,local_q) in enumerate(question_data):
                if canvas_q:
                    needs_update = True
                    # Does this question already have the correct hash
                    # and position?
                    local_hash = question_hash(local_q)
                    # print('i:', i)
                    # print('local hash:', local_hash)
                    # print('local text:', local_q['question_text'][:100])
                    # print("canvas_q['id']", canvas_q['id'])
                    # print("canvas_q['id'] in meta_questions:",
                    #       canvas_q['id'] in meta_questions)
                    if canvas_q['id'] in meta_questions:
                        meta_q = meta_questions[canvas_q['id']]
                        # print("meta_q['id']", meta_q['id'])
                        # print("meta_q['position']", meta_q['position'])
                        # print("meta_q['local_hash']", meta_q['local_hash'])
                        # print(
                        #     "meta_q['local_hash']==local_hash and"
                        #     " meta_q['position']==i+1 -->",
                        #     (meta_q['local_hash']==local_hash and
                        #      meta_q['position']==i+1)
                        # )
                        if (meta_q['local_hash']==local_hash and
                            meta_q['position']==i+1):
                            log.debug('  question (%s) needs no update',
                                      meta_q['id'])
                            needs_update = False

                    if needs_update:
                        # for a in local_q['answers']:
                        #     a['answer_text'] = a.pop('answer_html')
                        log.debug(local_q)
                        log.info(
                            'Updating question: %s (%s)',
                            canvas_q['question_text'][:100], canvas_q['id']
                        )
                        any_question_changed = True
                        if not self.dry_run:
                            # canvas_q.destroy()
                            # local_q['position'] = i+1
                            # canvas_q = quiz.new_question(**local_q)
                            canvas_q.update(**local_q)
                            canvas_q.sync()
                        else:
                            log.info('  ... DRY RUN ... not done.')
                    if not self.dry_run:
                        question_metas.append(
                            self.canvas_metadata(
                                canvas_q, question_content(local_q)
                            )
                        )

                else:
                    # Create new questions
                    log.info('Creating question: %s',
                             local_q['question_text'][:20])
                    any_question_changed = True
                    if not self.dry_run:
                        new_canvas_q = quiz.new_question(**local_q)
                        question_metas.append(
                            self.canvas_metadata(
                                new_canvas_q, question_content(local_q)
                            )
                        )
                    else:
                        log.info('  ... DRY RUN ... not done.')

            log.info('Updating question count for quiz: %s (%s)',
                     quiz['title'],quiz['id'])
            if not self.dry_run:
                quiz['question_count'] = len(local_questions)
                quiz.sync()
                meta = self.canvas_metadata(
                    quiz, config_content(config)
                )
                meta['questions'] = question_metas
                yaml.write_yaml(meta, meta_path)
            else:
                log.info('  ... DRY RUN ... not done.')

            if not any_question_changed:
                log.info('No questions changed for quiz: %s (%s).',
                         quiz['title'], quiz['id'])

        if not (new_quizzes or quizzes_to_update):
            log.info('No new quizzes or quizzes to update.')

    def sync_syllabus(self):
        path = self.dirs.syllabus_md
        if path.exists():
            html = self.course.markdown(self.template(path))
            self.course['syllabus_body'] = html
            log.info('Syncing syllabus')
            if not self.dry_run:
                self.course.sync()

    def sync_modules(self, paths=None):
        master_path = get_paths(paths) if paths else self.dirs.modules_master
        if not master_path:
            log.info('No modules to sync.')
        master_meta_path = self.dirs.metadata_path(master_path)

        def item_type(item):
            if 'page' in item:
                return 'Page'
            elif 'quiz' in item:
                return 'Quiz'
            elif 'assignment' in item:
                return 'Assignment'
            else:
                raise AttributeError('Unknown item type')

        master_meta = (yaml.read_yaml(master_meta_path)
                       if master_meta_path.exists() else None)
        if master_meta:
            local_hash = self.hash(Path(master_path).read_bytes())
            if master_meta['local_hash'] == local_hash:
                log.info('No changes to modules.')
                return

        # For now, rather than try to keep local and cloud consistent,
        # just destroy cloud version and replace with new modules.
        for m in self.course.modules:
            log.info('Destroying module: %s',m['name'])
            if not self.dry_run:
                m.destroy()

        pages = self.course.pages
        quizzes = self.course.quizzes
        assignments = self.course.nonquiz_assignments

        master_yaml = yaml.read_yaml(master_path)
        module_configs = OrderedDict()
        for data in master_yaml:
            module_configs[data['name']] = data
            for item in data.get('items',[]):
                itype = item_type(item)
                item['type'] = itype
                if itype == 'Page':
                    pfname = item.pop('page')
                    page_path = [p for p in self.dirs.pages
                                 if p.name == pfname][0]
                    html = self._md.mdconvert(self.template(page_path))
                    title = html.metadata['title']
                    page = [p for p in pages if p['title']==title][0]
                    item['canvas_page'] = page
                elif itype == 'Quiz':
                    qfname = item.pop('quiz')
                    path = [p for p in self.dirs.quizzes
                            if p.name == qfname][0]
                    config = yaml.read_yaml(path)
                    title = config['title']
                    quiz = [q for q in quizzes if q['title']==title][0]
                    item['canvas_content'] = quiz
                elif itype == 'Assignment':
                    afname = item.pop('assignment')
                    path = [p for p in self.dirs.assignments
                            if p.name == afname][0]
                    html = self._md.mdconvert(self.template(path))
                    name = html.metadata['name']
                    assignment = [a for a in assignments if a['name']==name][0]
                    item['canvas_content'] = assignment

        module_names = list(module_configs)

        positions = {name: i for i,name in enumerate(module_configs)}

        # Create the new modules
        for i, name in enumerate(module_names):
            modinfo = module_configs[name]
            # get all settings in the module config that isn't the
            # list of module items
            attrs = {k:v for k,v in modinfo.items() if k!='items'}
            attrs['position'] = i+1
            log.info('Creating module: %s', name)
            if not self.dry_run: 
                new_module = self.course.new_module(**attrs)

        modules = sorted(self.course.modules,
                         key=lambda m:module_names.index(m['name']))

        meta = []

        for i, ((name,modinfo), module) in enumerate(zip(module_configs.items(),
                                                         modules)):
            canvas_items = []
            meta.append({'module': CommentedMap(sorted(dict(module).items())),
                         'items': canvas_items})
            for j, item in enumerate(modinfo.get('items',[])):
                itype = item['type']
                kwargs = item.copy()
                kwargs['position'] = j+1

                if itype == 'Page':
                    page = kwargs.pop('canvas_page')
                    url = page['url']
                    kwargs['page_url'] = url
                elif itype in {'Assignment','Quiz','File','Discussion'}:
                    content = kwargs.pop('canvas_content')
                    kwargs['content_id'] = content['id']

                log.info('Creating module item: %s', kwargs['type'])
                if not self.dry_run:
                    new_item = module.new_item(**kwargs)
                    canvas_items.append(CommentedMap(
                        sorted(dict(new_item).items())
                    ))

        meta = CommentedMap([
            ('modules', meta),
            ('local_hash', self.hash(Path(master_path).read_bytes())),
        ])
        yaml.write_yaml(meta, master_meta_path)
            

    def slide_md(self, path):
        path = Path(path)
        slide_paths = self.dirs.slides_md

        md_path = next(filter(lambda p:p.name == path.name, slide_paths))
        return md_path.read_text()


    # @memoize
    # def get_course_files(self):
    #     return self.course.files

    def code_section(self, path, lines=()):
        if Path(path).is_absolute():
            path = Path(path).expanduser().resolve()
        else:
            path = Path(self.dirs.course_dir, path).expanduser().resolve()

        extra = ''
        if lines:
            llist = []
            for v in lines:
                if type(v) is int:
                    llist.append(v)
                else:
                    llist.extend(v)
            extra = f'''hl_lines="{' '.join(str(v) for v in llist)}"'''
            
        log.debug('Inserting code from {}'.format(path))
        if lines is None:
            return f'```python3\n{path.read_text()}\n```'
        else:
            return f'```\n#!python3 {extra}\n{path.read_text()}\n```'

    def slide_link(self, path, link_text=None):
        log.debug('Creating slide link: %s', path)
        path = Path(path)
        slide_paths = self.dirs.slides_html

        course_file = next(filter(lambda f:f['filename']==path.name,
                                  self.course.files))
        url = course_file['url']
        title = course_file['display_name']
        link_text = link_text or path.name
        
        anchor = '''<a class="instructure_file_link" title="{title}" href="{url}&amp;wrap=1">{link_text}</a>'''
        return anchor.format(url=url, title=title, link_text=link_text)

    def image_link(self, path, alt_text=None, style=None):
        log.debug('Creating image link: %s', path)
        if Path(path).is_absolute():
            path = Path(path).expanduser().resolve()
        else:
            path = Path(self.dirs.course_dir, path).expanduser().resolve()
        files = list(filter(lambda f:f['filename']==path.name,
                            self.course.files))
        if not files or self.force_upload:
            if not self.dry_run:
                if files:
                    course_file = files[0]
                    self.files_to_upload.add(path)
                else:
                    log.info('Uploading image: %s', path)
                    course_file = self.course.upload_file(path)
            else:
                course_file = {'url':'PLACEHOLDER_URL'}
        else:
            course_file = files[0]

        url = course_file['url']
        alt_text = alt_text or path.name
        
        def get_style(d):
            if d:
                return 'style="{}"'.format(
                    ' '.join(f'{k}: {v};' for k,v in sorted(d.items()))
                    if d else ''
                )
        return (f'<div style="margin: 10px"><img src="{url}"'
                f' alt="{alt_text}" {get_style(style)} /></div>')

    def image_anchor(self, path, url, alt_text=None):
        log.debug('Creating image anchor: %s', path)
        if Path(path).is_absolute():
            path = Path(path).expanduser().resolve()
        else:
            path = Path(self.dirs.course_dir, path).expanduser().resolve()
        files = list(filter(lambda f:f['filename']==path.name,
                            self.course.files))
        if not files or self.force_upload:
            if not self.dry_run:
                if files:
                    course_file = files[0]
                    self.files_to_upload.add(path)
                else:
                    log.info('Uploading image: %s', path)
                    course_file = self.course.upload_file(path)
            else:
                course_file = {'url':'PLACEHOLDER_URL'}
        else:
            course_file = files[0]
        img_url = course_file['url']
        alt_text = alt_text or path.name

        return (f'<a target="{alt_text}" href="{url}"><img src="{img_url}"'
                f' alt="{alt_text}" /></a>')

    def page_link(self, path, link_text=None, ref=None):
        log.debug('Creating page link: %s', path)
        path = Path(path)
        if not path.is_absolute():
            path = Path(self.dirs.course_dir, path).expanduser().resolve()
        md_path = self.dirs.metadata_path(path, create=False)
        if not md_path.exists():
            return link_text
        md = yaml.read_yaml(md_path)
        # pages = self.course.pages

        anchor = '''<a title="{title}" href="{href}{ref}">{link_text}</a>'''
        return anchor.format(
            title=md['title'], href=md['html_url'], link_text=link_text,
            ref=f'#{ref}' if ref else '',
        )

    def assign_link(self, path, link_text=None):
        log.debug('Creating assignment link: %s', path)
        path = Path(path)
        if not path.is_absolute():
            path = Path(self.dirs.course_dir, path).expanduser().resolve()
        md_path = self.dirs.metadata_path(path, create=False)
        if not md_path.exists():
            log.info("  assignment doesn't exist yet in Canvas")
            return link_text

        md = yaml.read_yaml(md_path)
        # pages = self.course.pages

        anchor = '''<a title="{title}" href="{href}">{link_text}</a>'''
        try:
            return anchor.format(
                title=md['name'], href=md['html_url'], link_text=link_text,
            )
        except:
            pprint.pprint(dict(md))
            raise

    def file_link(self, path, link_text=None, force=False):
        log.debug('Creating file link: %s', path)
        if Path(path).is_absolute():
            path = Path(path).expanduser().resolve()
        else:
            path = Path(self.dirs.course_dir, path).expanduser().resolve()
        files = list(filter(lambda f:f['filename']==path.name,
                            self.course.files))
        if not files or self.force_upload or force:
            if not self.dry_run:
                if files:
                    log.info('FILES: {}'.format(files))
                    course_file = files[0]
                    self.files_to_upload.add(path)
                else:
                    log.info('Uploading file from file_link: %s', path)
                    course_file = self.course.upload_file(path)
            else:
                course_file = {'url':'PLACEHOLDER_URL'}
        else:
            course_file = files[0]
        url = course_file['url']
        link_text = link_text or path.name
        
        anchor = ('<a class="instructure_file_link" title="{title}"'
                  ' href="{url}&amp;wrap=1">{link_text}</a>')
        return anchor.format(url=url, title=path.name, link_text=link_text)

    def canvas_file_link(self, canvas_path, link_text=None):
        path = Path(canvas_path)
        uq = urllib.parse.unquote_plus
        course_file = next(filter(lambda f:uq(str(f.path))==str(path), self.course.files))
        anchor = '''<a class="instructure_file_link" title="{title}" href="{url}&amp;wrap=1">{link_text}</a>'''.format(
            title=path.name, url=course_file['url'], link_text=link_text
        )
        return anchor

    def shuffle(self, values, seed=None, rnd=None):
        return self.random.sample(values, len(values))
        # R = self.random(seed) if rnd is None else rnd
        # return R.sample(values, len(values))

    def shuffled(self, values, seed=None):
        return self.random.sample(values, len(values))

    def questions(self, path):
        name = Path(path).name
        path = None
        for p in self.dirs.quizzes:
            if p.name == name:
                path = p
        if not path:
            log.error(f'quiz ({name}) not found!')
        quiz = yaml.load(self.template(path))
        questions = quiz['questions']
        return questions

    def take(self, comp_seq, start, stop=None, step=None):
        seq = list(comp_seq)
        if start > 0 and start < 1:
            # Random percentage of seq
            return self.random.sample(
                seq, int(len(seq) * start) or 1
            )
        else:
            if not step:
                step = 1
            if not stop:
                stop = start
                start = 0
            return seq[start:stop:step]

    def to_yaml(self, obj):
        if isinstance(obj,types.GeneratorType):
            obj = list(obj)
        return yaml.dump(obj)

    def template(self, path):
        log.debug('Parsing through template: %s', path)
        path = Path(path).resolve()
        text = path.read_text()
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(str(path.parent))
        )

        env.globals['slide_link'] = self.slide_link
        env.globals['page_link'] = self.page_link
        env.globals['assign_link'] = self.assign_link
        env.globals['slide_md'] = self.slide_md
        env.globals['file_link'] = self.file_link
        env.globals['image_link'] = self.image_link
        env.globals['image_anchor'] = self.image_anchor
        env.globals['questions'] = self.questions
        env.globals['shuffled'] = self.shuffled
        env.globals['code_section'] = self.code_section
        env.globals['course'] = '-'.join(
            map(str, (self.course.id.year,self.course.id.period,
                      self.course.id.section))
        )

        env.filters['shuffle'] = self.shuffle
        env.filters['take'] = self.take
        env.filters['to_yaml'] = self.to_yaml

        template = env.get_template(path.name)

        content = template.render()

        return content
    
            
    
def merge_paths(paths):
    pass

def initialize_course(course, root_dir):
    manager = CourseManagement(course, root_dir)
    manager.initialize()

def sync_course(course, root_dir):
    manager = CourseManagement(course, root_dir)
    manager.sync_slides()
    manager.sync_pages()
