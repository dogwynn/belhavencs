import re
import sys
import time
import warnings
import traceback
import inspect
import functools
import importlib
import unittest
from unittest.mock import (
    patch,
)
from unittest.signals import registerResult
from functools import wraps
import errno
import os
import signal
import textwrap

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

frames = []
def check(*a,**kw):
    f = inspect.currentframe()
    frames.append(f)
def pop_frame():
    f = frames.pop()
    prev = f.f_back
    f.clear()
    return prev
def clear_frames():
    global frames
    for f in frames:
        f.clear()
    frames = []
    

def cleanup_frames(function):
    @functools.wraps(function)
    def wrap(*a,**kw):
        retval = function(*a,**kw)
        clear_frames()
        return retval
    return wrap

def no_traceback(function):
    function.no_traceback = True
    @functools.wraps(function)
    def wrap(*a,**kw):
        retval = function(*a,**kw)
        return retval
    return wrap
    
def just_msg(function):
    function.just_msg = True
    @functools.wraps(function)
    def wrap(*a,**kw):
        retval = function(*a,**kw)
        return retval
    return wrap
    

def skip(func):
    func.skip = True
    return func

def check_func_skip(checked_name):
    def outer(function):
        @functools.wraps(function)
        def wrap(*a, **kw):
            mod_name, func_name = checked_name.rsplit('.',1)
            module = importlib.import_module(mod_name)
            if hasattr(module,func_name):
                if hasattr(getattr(module,func_name), 'skip'):
                    raise unittest.SkipTest('Skipping function: {}'.format(func_name))
            retval = function(*a, **kw)
            return retval
        return wrap
    return outer

def true(v):
    frames.append(inspect.currentframe())
    return bool(v)

def equals(val, check):
    frames.append(inspect.currentframe())
    return val == check

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class TestResult(unittest.TestResult):
    """A test result class that can print formatted text results to a
    stream.

    Used by TextTestRunner.

    """
    separator1 = '=' * 70
    separator2 = '-' * 70
    first_fail = None
    all_failures = None
    def __init__(self, stream, show_descriptions, verbosity):
        super().__init__(stream, show_descriptions, verbosity)
        self.stream = stream
        self.show_descriptions = show_descriptions
        self.all_failures = []

    def get_test_doc(self, test):
        paras = []
        doc = test._testMethodDoc
        short_done = False
        indent = None
        if doc:
            lines = doc.splitlines()
            p = lines[0]
            for line in lines[1:]:
                if line.strip():
                    if short_done:
                        if indent is None:
                            indent = re.search(r'\S',line).span()[0]
                        p  += line[indent:] + '\n'
                    else:
                        p += ' ' + line.strip()
                else:
                    if p:
                        paras.append(p)
                    if not short_done:
                        short_done = True
                    p = ''
            if p:
                paras.append(p)
        else:
            paras = [str(test)]
        return paras

    def getDescription(self, test):
        short, *extended = self.get_test_doc(test)
        if self.show_descriptions and short:
            return short
        else:
            return str(test)

    def startTest(self, test):
        super().startTest(test)

    def test_header(self, test):
        header = self.getDescription(test) + " ... "
        return header

    def success_text(self, text):
        return bcolors.BOLD + bcolors.OKGREEN + text + bcolors.ENDC
    def skip_text(self, text):
        return bcolors.BOLD + bcolors.OKBLUE + text + bcolors.ENDC
    def fail_text(self, text):
        return bcolors.BOLD + bcolors.FAIL + text + bcolors.ENDC

    def set_first_fail(self, test, flavor, err):
        if self.first_fail:
            self.all_failures.append((test, flavor, err))
        else:
            self.first_fail = (test, flavor, err)

    def addSuccess(self, test):
        super().addSuccess(test)
        self.stream.write('.')
        self.stream.flush()

    def addError(self, test, err):
        super().addError(test, err)
        self.stream.write(self.fail_text('E'))
        self.stream.flush()
        self.set_first_fail(test, 'Test Has an Error', err)

    def addFailure(self, test, err):
        super().addFailure(test, err)
        self.stream.write(self.fail_text('F'))
        self.stream.flush()
        self.set_first_fail(test, 'Test Has Failed', err)

    def addSkip(self, test, reason):
        super().addSkip(test, reason)
        self.stream.write(self.skip_text('s'))
        self.stream.flush()

    def addExpectedFailure(self, test, err):
        super().addExpectedFailure(test, err)
        self.stream.write(self.skip_text('x'))
        self.stream.flush()

    def addUnexpectedSuccess(self, test):
        super().addUnexpectedSuccess(test)
        self.stream.write(self.skip_text('u'))
        self.stream.flush()

    def print_fail(self, err):
        self.stream.writeln(''.join(traceback.format_exception(*err)))
        # if not self.shown_first:
        #     self.stream
        #     self.shown_first = True
        

    def printErrors(self):
        failures = [self.first_fail]+self.all_failures
        if failures:
            for test, flavor, err in failures: #[:1]:
                short, *extended = self.get_test_doc(test)
                self.stream.writeln()
                self.stream.write(bcolors.FAIL)
                self.stream.writeln(self.separator1)
                self.stream.writeln(
                    textwrap.fill("{}: {}".format(flavor,short))
                )
                if (short or extended) and ('fail' in flavor.lower()):
                    self.stream.writeln()
                    # self.stream.write(bcolors.FAIL)
                    # self.stream.writeln(self.separator1)
                    self.stream.writeln('\n'.join(extended))
                    self.stream.writeln(self.separator2)
                    self.stream.write(bcolors.ENDC)
                    method = getattr(test,getattr(test,'_testMethodName'))
                    if not hasattr(method,'no_traceback'):
                        if hasattr(method, 'just_msg'):
                            self.print_fail(err.msg)
                        else:
                            self.print_fail(err)
                    self.stream.write(bcolors.ENDC)
                elif ('error' in flavor.lower()):
                    self.stream.writeln(self.separator2)
                    self.stream.write(bcolors.ENDC)
                    self.print_fail(err)
                    self.stream.write(bcolors.ENDC)
        # if self.dots or self.showAll:
        #     self.stream.writeln()
        # self.printErrorList('ERROR', self.errors)
        # self.printErrorList('FAIL', self.failures)



class TestRunner:
    """A test runner class that displays results in textual form.

    It prints out the names of tests as they are run, errors as they
    occur, and a summary of the results at the end of the test run.
    """
    resultclass = TestResult

    def __init__(self, stream=None, descriptions=True, verbosity=2,
                 failfast=False, buffer=False, resultclass=None, warnings=None):
        if stream is None:
            stream = sys.stderr
        self.stream = unittest.runner._WritelnDecorator(stream)
        self.descriptions = descriptions
        self.verbosity = verbosity
        self.failfast = failfast
        self.buffer = buffer
        self.warnings = warnings
        if resultclass is not None:
            self.resultclass = resultclass

    def _makeResult(self):
        return self.resultclass(self.stream, self.descriptions, self.verbosity)

    def run(self, test):
        "Run the given test case or test suite."
        result = self._makeResult()
        registerResult(result)
        result.failfast = self.failfast
        result.buffer = self.buffer
        with warnings.catch_warnings():
            if self.warnings:
                # if self.warnings is set, use it to filter all the warnings
                warnings.simplefilter(self.warnings)
                # if the filter is 'default' or 'always', special-case the
                # warnings from the deprecated unittest methods to show them
                # no more than once per module, because they can be fairly
                # noisy.  The -Wd and -Wa flags can be used to bypass this
                # only when self.warnings is None.
                if self.warnings in ['default', 'always']:
                    warnings.filterwarnings('module',
                            category=DeprecationWarning,
                            message='Please use assert\w+ instead.')
            startTime = time.time()
            startTestRun = getattr(result, 'startTestRun', None)
            if startTestRun is not None:
                startTestRun()
            try:
                test(result)
            finally:
                stopTestRun = getattr(result, 'stopTestRun', None)
                if stopTestRun is not None:
                    stopTestRun()
            stopTime = time.time()
        timeTaken = stopTime - startTime

        result.printErrors()

        self.stream.writeln()

        if True:
            if hasattr(result, 'separator2'):
                self.stream.writeln(result.separator2)
            run = result.testsRun
            self.stream.writeln("Ran %d test%s in %.3fs" %
                                (run, run != 1 and "s" or "", timeTaken))
            self.stream.writeln()

            expectedFails = unexpectedSuccesses = skipped = 0
            try:
                results = map(len, (result.expectedFailures,
                                    result.unexpectedSuccesses,
                                    result.skipped))
            except AttributeError:
                pass
            else:
                expectedFails, unexpectedSuccesses, skipped = results

            infos = []
            if not result.wasSuccessful():
                self.stream.write("FAILED")
                failed, errored = len(result.failures), len(result.errors)
                if failed:
                    infos.append("failures=%d" % failed)
                if errored:
                    infos.append("errors=%d" % errored)
            else:
                self.stream.write("ALL TESTS PASSED")
            if skipped:
                infos.append("skipped=%d" % skipped)
            if expectedFails:
                infos.append("expected failures=%d" % expectedFails)
            if unexpectedSuccesses:
                infos.append("unexpected successes=%d" % unexpectedSuccesses)
            if infos:
                self.stream.writeln(" (%s)" % (", ".join(infos),))
            else:
                self.stream.write("\n")
            self.stream.write(bcolors.ENDC)
        return result


class Exercise:
    exercises = []
    nexercises = None  # should be set to an integer by the subclass
    def __init__(self):
        frame = inspect.currentframe()
        try:
            self.globals = frame.f_back.f_back.f_globals
            self.locals = frame.f_back.f_back.f_locals
        finally:
            del frame
        Exercise.exercises.append(self)

    def eval(self, expr):
        return eval(expr, self.globals, self.locals)

    def run_test(self, test_index):
        assert False

    def describe_test(self, test_index):
        return ''

    def test_generator(self):
        for i in range(self.nexercises):
            yield self.test_case(i)

    @staticmethod
    def number_of_exercises():
        return sum(k.nexercises for k in Exercise.exercises)

            
class ExerciseTrue(Exercise):
    def __init__(self, *boolean_expressions):
        super().__init__()
        self.boolean_expressions = boolean_expressions
        self.nexercises = len(boolean_expressions)

    def describe_test(self, test_index):
        return self.boolean_expressions[test_index]

    def test_case(self, test_index):
        ex = self
        class TestTrue(unittest.TestCase):
            def test_true(self):
                expr = ex.boolean_expressions[test_index]
                self.assertTrue(ex.eval(expr) )
            test_true.__doc__ = '''test true: 
            
            Expression: "{}" does not evaluate to true
            '''.format(ex.boolean_expressions[test_index])
        return TestTrue

    def run_test(self, test_index):
        assert self.eval(self.boolean_expressions[test_index]) == True
true = ExerciseTrue

class ExerciseEquals(Exercise):
    def __init__(self, *pairs):
        super().__init__()
        self.pairs = pairs
        self.nexercises = len(pairs)

    def test_case(self, test_index):
        expr, value = self.pairs[test_index]
        ex = self
        class TestEquals(unittest.TestCase):
            def test_equals(self):
                self.assertEquals(ex.eval(expr),value )
            test_equals.__doc__ = '''test equals: 
            
            Expression: {} does not equal {}
            '''.format(expr, value)
        return TestEquals

equals = ExerciseEquals

def exercise_test_cases():
    cases = []
    for ex in Exercise.exercises:
        cases.extend(ex.test_generator())
    return cases
    
def run():
    loader = unittest.TestLoader()

    cases = exercise_test_cases()
    
    suite = unittest.TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)

    results = TestRunner().run(suite)
    return results

