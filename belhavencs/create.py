import itertools
import random
import json
import sys
import string
import textwrap
import collections

import jinja2

def shuffled(seq):
    copy = list(seq)[:]
    random.shuffle(copy)
    return copy

class ParamGenerator:
    type_map = {
        list: (lambda:[random.randrange(-10,10) for i in range(20)], "a list"),
        int: (lambda:random.randrange(-10,10), "an integer"),

        str: (lambda:''.join(random.choice(string.ascii_letters)
                             for i in range(20)), 'a string'),
        dict: (lambda:{k: random.choice([k, 10*k, chr((k%(128-32))+32)])
                       for k in range(20)}, 'a dictionary'),
    }
    def __init__(self, type_map=None):
        self.type_map = self.__class__.type_map.copy()
        self.type_map.update(type_map if type_map else {})

    def __call__(self, function_index, otype):
        return self.type_map.get(function_index, {}).get(
            otype, self.type_map[otype]
        )
        
class VariableSets:
    variables = {
        list: ['A','B','C','L','mylist',],
        int: ['a','b','c','d','x','y','z','myvar',],
        str: ['mystr','string','str_value','s'],
        dict: ['D','E','F','mydict',],
    }
    def __init__(self, variables=None):
        self.variables = {k:v.copy()
                          for k,v in self.__class__.variables.items()}
        self.variables.update(variables if variables else {})

    @property
    def new(self):
        return {k:shuffled(v) for k,v in self.variables.items()}


func_temp = '''


#----------------------------------------------------------------------
# Problem {{i}}
#----------------------------------------------------------------------
#
{{ create }}
#
# It should:
#
{{ steps }}
#----------------------------------------------------------------------

{{ code }}

'''

test_template = '''

class {{ test_name }}(unittest.TestCase):
    def test000_existence(self):
        """ {{ name }}: testing existence

        Function {{ name }} doesn't exist """
        self.assertTrue(hasattr({{ module }}, "{{name}}"))
        self.assertTrue(callable({{ module }}.{{name}}))

    def test001_sig(self):
        """ {{ name }}: testing signature

        Parameters ({{ params|join(', ') }}) are missing """
        sig = inspect.signature({{ module }}.{{name}})
        self.assertEqual(set({{ param_list }}),
                         set(sig.parameters.keys()))

'''

test_eval_template = '''
    def test{{i}}_eval(self):
        """ {{ name }}: testing return value

        Function {{name}} does not return the correct value
        for input parameters:

        {{ test_params_str }}

        should return

        {{ test_value }} """
        value = timeout(1,"You have an infinite loop!!!")({{ module }}.{{name}})(*{{ test_params }})

        self.assertEqual(value, {{ value }})
'''

test_file_template = '''
import unittest
import inspect

from functools import wraps
import errno
import os
import signal

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

from belhavencs import harness

import {{ module }}

{{ tests|join('\n') }}

def run():
    loader = unittest.TestLoader()

    cases = {{ test_names|join(',') }}

    suite = unittest.TestSuite()
    for case in cases:
        tests = loader.loadTestsFromTestCase(case)
        suite.addTests(tests)

    results = harness.TestRunner().run(suite)
    return results

if __name__=='__main__':
    run()

'''

header_template = '''#!/usr/bin/env {{ python_version }}
'''

footer_template = '''






if __name__=='__main__':
    import {{ test_module }}
    {{ test_module }}.run()
'''


FunctionSpec = collections.namedtuple(
    'FunctionSpec', ['parameters','internal_variables', 'return_types',
                     'evaluation', 'steps', 'solution']
)

class SpecList(collections.OrderedDict):
    pass

class FunctionAssignment:
    def __init__(self, name: str, specs: SpecList,
                 param_generator: ParamGenerator,
                 variable_sets: VariableSets,
                 salt: str='', tests_per_function:int=5):
        '''Generates a per-student set of function-oriented assigments,
        complete with a testing harness for students to evaluate their
        progress.

        Args:

        name: name of the module (e.g. "hw5", "lab10")

        specs: SpecList of function specifications

        param_generator: ParamGenerator object to generate parameters
           for this assignment

        variable_sets: VariableSets object to generate
           parameter/internal variable names for this assignment

        '''
        self.name = name
        self.specs = specs
        self.param_generator = param_generator
        self.variable_sets = variable_sets

        self.salt = salt
        self.tests_per_function = tests_per_function

    def format_args(self, params, internals):
        # Convert a list of parameter and internal variable values to
        # a dictionary:
        #
        #  {'p1': '"string_parameter"',
        #   'p2': "['list','parameter']",
        #   'v1': "'internal_string_variable'",
        #   'v2': "['an','internal','list','variable']"}
        #
        # The values in the dictionary are their repr-ed version, so
        # that they can be provided to a Jinja2 template.
        # 
        args = {'p{}'.format(i+1):repr(v) for i,v in enumerate(params)}
        args.update(
            {'v{}'.format(i+1):repr(v) for i,v in enumerate(internals)}
        )
        return args
        
    def format_variables(self, params,internals):
        # Convert a list of parameter and internal variable names to a
        # dictionary:
        #
        #  {'p1': 'mystr',
        #   'p2': 'A',
        #   'v1': 's',
        #   'v2': 'mylist'}
        #
        # This will be used to populate solution code templates with
        # the randomly selected parameters and internal variable
        # names.
        # 
        args = {'p{}'.format(i+1):v for i,v in enumerate(params)}
        args.update(
            {'v{}'.format(i+1):v for i,v in enumerate(internals)}
        )
        return args

    def solution_code(self, solution_template: str, function_name: str,
                      params: list, internals: list):
        # Render code solution from a Jinja2 template
        variables = self.format_variables(params, internals)
        return jinja2.Template(solution_template).render(
            name=function_name, params=params, **variables
        )

    @property
    def module_filename(self):
        return '{}.py'.format(self.name)

    @property
    def test_module(self):
        return '{}_test'.format(self.name)

    @property
    def test_module_filename(self):
        return '{}.py'.format(self.test_module())
        
    header_template = header_template
    python_version = 'python3.4'

    def header(self, template=None, python_version=None, **kw):
        # Generate header for assignment module
        #
        # Args: 
        template = template if template else self.header_template

        python_version = python_version if python_version else (
            self.python_version
        )

        text = jinja2.Template(template).render(
            python_version=python_version, **kw
        )
        return text

    footer_template = footer_template

    def footer(self, template=None, test_module=None, **kw):
        # Generate footer for assignment module
        footer_template = template if template else self.footer_template

        test_module = test_module if test_module else self.test_module()

        text = jinja2.Template(template).render(
            test_module=self.test_module(), **kw
        )

        return text

    def test_name(self, problem_index):
        # Generate test name
        return 'Test{}'.format(problem_index)

    def test_parameters(self, spec_index: int, param_types: list,
                        eval_expression: str):
        # Format the test parameters
        test_params = []
        for i in range(self.tests_per_function):
            test_params = []
            for i,ptype in enumerate(param_types):
                p_value_func, p_desc = self.param_generator(spec_index,ptype)
                param_value = p_value_func()
                test_params.append(param_value)

            formatted_args = format_args(test_params,internals)
            try:
                formatted_expr = eval_expression.format(**formatted_args)
                test_value = eval(formatted_expr)
            except:
                print('Function template:',spec_index)
                print('Args:',formatted_args)
                print('Raw expression:',repr(eval_expression))
                print('Expression:',repr(formatted_expr))
                raise
            test_params.append((test_params,test_value))
        return test_params

    test_template = test_template
    def test_base(self, template=None, **kw):
        # Generate base code for test case from Jinja2 template
        template = template if template else self.test_template
        text = jinja2.Template(template).render(**kw)
        return text

    test_eval_template = test_eval_template
    def test_eval(self, template=None, **kw):
        template = template if template else self.test_eval_template
        text = jinja2.Template(template).render(**kw)
        return text
        
    def test_case(self, spec_index:int, problem_index:int, function_name:str,
                  params:list, param_types:list, internals:list,
                  internal_types:list, eval_expression:str):

        test_params = self.test_parameters(spec_index, param_types,
                                           eval_expression)

        tname = self.test_name(problem_index)

        # Generate base code for test case
        tbase = self.test_base(
            test_name=tname, name=function_name, params=params,
            param_list=repr(params),
        )

        # For each set of test parameters (# equal to
        # self.tests_per_funciton), add a test case from template
        for i,(tparams,tvalue) in enumerate(test_params):
            test_params_str = '\n        '.join(
                '{} = {}'.format(n,repr(v)) for n,v in zip(params,tparams)
            )
            teval = self.test_eval(
                i=i+2, test_params=repr(tparams), test_value=tvalue,
                test_params_str=test_params_str,
                name=function_name,
                value=repr(tvalue),
            )
            tbase += teval

        return (tname, tbase)
