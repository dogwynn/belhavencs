from pathlib import Path

def walk(pth=Path('.'), pattern='*', only_file=True) :
    '''list all files in pth matching a given pattern, can also list dirs
    if only_file is False

    '''
    if pth.match(pattern) and not (only_file and pth.is_dir()) :
        yield pth
    for sub in pth.iterdir():
        if sub.is_dir():
            yield from walk(sub, pattern, only_file)
        else:
            if sub.match(pattern) :
                yield sub

