from datetime import datetime
from pathlib import Path
import csv

from ruamel.yaml.comments import CommentedMap

from .yaml import read_yaml, write_yaml
from .logging import new_log

log = new_log('directory')

def default_student(student=None):
    smap = CommentedMap()
    for key in ['name','belhaven_name','sortable_name','id','belhaven_id',
                'email','canvas_id','github','freecodecamp',
                'bitbucket','pythonanywhere',
                'password','password_hash', 'phone']:
        smap[key] = ''
    if student:
        smap.update(student)
    return smap
    
stub_student = default_student({
    'name': 'Belhaven CS',
    'belhaven_name': 'Belhaven CS',
    'id': 'belhavencs',
    'belhaven_id': None ,
    'email': None,
    'canvas_id': None,
    'github': 'belhavencs',
    'pythonanywhere': 'belhavencs',
})

class StudentDB(object):
    def __init__(self, root):
        self.root = Path(root).expanduser().resolve()
        self.path = Path(self.root, 'students.yml')
        self.db = None

        if self.path.exists():
            self.db = read_yaml(self.path)
        else:
            self.db = CommentedMap()
            self.db['stub_student'] = stub_student
            self.db['students'] = []

        self.needs_sync = False

    def map_from_canvas(self, student):
        name = student['name']
        first, *middle, last = name.lower().split()
        smap = default_student({
            'name': name,
            'belhaven_name': name,
            'sortable_name': student['sortable_name'],
            'id': first[0]+last,
            'belhaven_id': student['login_id'],
            'email': student['login_id'] + '@students.belhaven.edu',
            'canvas_id': student['id'],
        })

        return smap

    def _append_student(self, student_map):
        self.db['students'].append(student_map)
        self.db['students'].sort(key=lambda s:s['sortable_name'])
        self.needs_sync = True

    def add_from_canvas(self, student):
        '''Idempotent add
        
        '''

        for s in self.db['students']:
            if s['canvas_id'] == student['id']:
                return s
        smap = self.map_from_canvas(student)
        self._append_student(smap)
        return smap

    def backup(self):
        fname = self.path.stem+datetime.now().strftime('%Y-%m-%d_%H%M')+'.yml'
        backup = Path(self.path.parent,fname)
        log.info('Backing up students DB: %s',backup)
        backup.write_text(self.path.read_text())

    def sync(self):
        if self.needs_sync:
            self.backup()
            write_yaml(self.db, self.path)
            self.needs_sync = False

    def write_csv(self, path, cols=None):
        if cols is None:
            cols = list(self.db['students'][0].keys())
        with open(path, 'w') as wfp:
            writer = csv.DictWriter(wfp, cols, extrasaction='ignore')
            writer.writeheader()
            writer.writerows(self.db['students'])
            
